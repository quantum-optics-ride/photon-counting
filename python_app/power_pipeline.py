"""
Launching of power measurements:
1. Compling cpp library that evaluates measurements on GPU
2. Setup of digitiser
3. Running of measurements
4. Real time update of graph
"""

import os
import re
import math
import time
import ctypes
import shutil
import threading

import numpy as np
from pyprind import ProgBar
import matplotlib.pyplot as plt
from watchdog.observers import Observer


from python_app.utils.terminal_colour import TerminalColour
from python_app.sp_digitiser import SpDigitiser
from python_app.utils.file_watcher import PlotTrigger
from python_app.utils import library_manager
from python_app.utils import gpu_utils
from python_app.g1_pipeline import G1Pipeline


class PowerPipeline:
    HEADING = (
        TerminalColour.CSKYBLUEBG
        + TerminalColour.UNDERLINE
        + "POWER-PIPELINE"
        + TerminalColour.ENDC
        + ":"
    )
    LOG_TEMPLATE = f"{HEADING:<31}{{info}}"

    NS_PER_POINT = 2.5

    LOG_LOCATION = "./libia.log"
    LIBRARY_LOCATION = "./csrc/bin/libia.so"
    DUMP_FOLDER = "./dump"
    STORE_FOLDER = "./store"

    @classmethod
    def log(cls, message: str):
        print(cls.LOG_TEMPLATE.format(info=str(message)))

    def __init__(self, ipython: bool, recompile: bool = True, **kwargs):
        self.ipython = ipython

        os.makedirs(self.DUMP_FOLDER, exist_ok=True)

        self.SP_POINTS = kwargs["SP_POINTS"]

        self.log("Building kernel with:\n" + f"SP_POINTS={self.SP_POINTS}")

        self.libia = self.build_library(
            self.SP_POINTS,
            recompile=recompile,
        )

        (self.fig, self.ax, self.plot_dict) = self.prepare_plot(
            self.SP_POINTS * self.NS_PER_POINT, self.SP_POINTS
        )

        # Prepare digitiser
        self.spd = SpDigitiser()

    @classmethod
    def build_library(cls, SP_POINTS: int, recompile: bool) -> ctypes.CDLL:

        if recompile:
            library_manager.build_library(
                {
                    "SP_POINTS": str(SP_POINTS),
                    "G1_DIGITISER_POINTS": str(G1Pipeline.G1_DIGITISER_POINTS),
                }
            )
        libia = ctypes.cdll.LoadLibrary(cls.LIBRARY_LOCATION)

        return libia

    @staticmethod
    def prepare_plot(time_in_ns: int, SP_POINTS: int) -> (plt.Figure, plt.Axes, dict):
        time_axis = np.linspace(0, time_in_ns, SP_POINTS)

        plot_dict = {
            "CHA": {"idx": 0, "ax": 0, "color": "red", "label": "CHA"},
            "CHASQ": {"idx": 2, "ax": 1, "color": "red", "label": "ChA$^2$"},
            "CHB": {"idx": 1, "ax": 2, "color": "blue", "label": "ChB"},
            "CHBSQ": {"idx": 3, "ax": 3, "color": "blue", "label": "ChB$^2$"},
            "SQ": {"idx": 4, "ax": 4, "color": "black", "label": "ChA$^2$ + ChB$^2$"},
        }

        # Prepare plot
        fig, ax = plt.subplots(5, 1, figsize=(8, 8), sharex=True)
        for ch in ["CHA", "CHB", "CHASQ", "CHBSQ", "SQ"]:
            i = plot_dict[ch]["ax"]

            # Plot line that will be constantly updateed
            (plot_dict[ch]["plot"],) = ax[i].plot(
                time_axis, [0] * SP_POINTS, color=plot_dict[ch]["color"]
            )
            # Labels
            ax[i].grid(color="#bfbfbf", linestyle="-", linewidth=1)
            ax[i].set_ylabel(plot_dict[ch]["label"], color=plot_dict[ch]["color"])
        ax[-1].set_xlabel("Time (ns)", fontsize=14)

        return (fig, ax, plot_dict)

    @classmethod
    def prepare_plot_trigger(cls):
        """
        Process will monitor the file folder and update the state of `PlotTrigger`.
        It's state will be polled in order to determine if plot should be updated
        """
        observer = Observer()
        plot_trigger = PlotTrigger()
        observer.schedule(plot_trigger, cls.DUMP_FOLDER)
        observer.start()

        return (observer, plot_trigger)

    def update_plot(self, data: np.array, run: int, NO_RUNS):
        """
        Using the supplied plotting handles and parameters, update the plot
        """

        for ch in ["CHA", "CHB", "CHASQ", "CHBSQ", "SQ"]:
            self.plot_dict[ch]["plot"].set_ydata(data[self.plot_dict[ch]["idx"]])
            self.ax[self.plot_dict[ch]["ax"]].relim()
            self.ax[self.plot_dict[ch]["ax"]].autoscale_view()

        self.fig.suptitle(
            "Run {run}/{no_runs}".format(
                run="{:_}".format(run), no_runs="{:_}".format(NO_RUNS)
            )
        )
        if self.ipython:
            self.fig.canvas.draw()
        else:
            plt.pause(0.1)
            plt.draw()  # non-blocking drawing

    def execute_run(
        self,
        digitiser_parameters: dict,
        run_name: str,
        NO_RUNS: int,
        chA_background: np.array = None,
        chB_background: np.array = None,
    ):
        if os.path.exists(self.LOG_LOCATION):
            os.remove(self.LOG_LOCATION)

        # Check that background arrays are of the correct type
        if chA_background is not None:
            assert (
                chA_background.dtype == np.short
            ), "chA_background must be of type np.short"
        else:
            chA_background = np.zeros(self.SP_POINTS, dtype=np.short)
        if chB_background is not None:
            assert (
                chB_background.dtype == np.short
            ), "chB_background must be of type np.short"
        else:
            chB_background = np.zeros(self.SP_POINTS, dtype=np.short)

        # Setup digitiser
        r_points = self.libia.get_r_points_requested_from_digitiser(
            self.spd.adq_cu_ptr, self.SP_POINTS, NO_RUNS
        )
        digitiser_parameters = {
            **digitiser_parameters,
            **{
                "r_points": r_points,
                "sp_points": self.SP_POINTS,
            },
        }
        self.spd.check_parameters(digitiser_parameters)
        self.spd.parameter_setup(digitiser_parameters)

        (observer, plot_trigger) = self.prepare_plot_trigger()

        # Launch measurements
        cpp_thread = threading.Thread(
            target=self.libia.run_power_measurements,
            name="Power Kernel Runner",
            args=(
                self.spd.adq_cu_ptr,
                chA_background.ctypes.data,
                chB_background.ctypes.data,
                self.SP_POINTS,
                NO_RUNS,
                ctypes.create_string_buffer(
                    f"{self.DUMP_FOLDER}/{run_name}".encode("utf-8"), size=40
                ),
            ),
        )
        cpp_thread.start()
        self.log("Measurements started")

        progress_bar = ProgBar(NO_RUNS, bar_char="█", width=100)
        plotted_run = 0
        while True:
            time.sleep(1)
            if plot_trigger.update:

                # 1. Get hold of the lock
                with plot_trigger.lock:
                    plot_trigger.update = False
                    filename = plot_trigger.filename

                # 2. Read run number
                with open(plot_trigger.filename, "r") as fin:
                    run_number = int(re.search("\d+", fin.readline()).group())

                # 3. If this is a new run (sometimes a trigger occurs mid-file) update plots
                if run_number != plotted_run:
                    plotted_run = run_number
                    data = np.transpose(np.loadtxt(filename))
                    self.update_plot(data, plotted_run, NO_RUNS)
                    progress_bar.update()

            if not cpp_thread.is_alive():
                observer.join(0)
                # Report on any log errors
                library_manager.try_to_read_log_file()
                break

        self.update_plot(
            np.transpose(np.loadtxt(plot_trigger.filename)), NO_RUNS, NO_RUNS
        )
        result_file = f"{self.STORE_FOLDER}/{run_name}.csv"
        shutil.copyfile(filename, result_file)
        self.log(f"Measurements done -> data dumped to {result_file}")
