from python_app.sp_digitiser import SpDigitiser
from python_app.power_pipeline import PowerPipeline
from python_app.g1_pipeline import G1Pipeline
from python_app.utils import file_ops
import os
import matplotlib.pyplot as plt


def power_measurement(
    sp_points: int,
    no_runs: int,
    run_name: str,
    chA_background=None,
    chB_background=None,
    recompile=False,
):

    # Setup
    pp = PowerPipeline(
        ipython=False,
        recompile=recompile,
        SP_POINTS=sp_points,
    )
    plt.draw()  # Non-blocking drawing
    plt.pause(0.001)

    # Execution
    pp.execute_run(
        NO_RUNS=no_runs,
        digitiser_parameters={
            "delay": 0,
            "trigger_type": SpDigitiser.TRIGGER_EXTERNAL,
            #     "trigger_type": SpDigitiser.TRIGGER_SOFTWARE,
            "channelA_gain": 1,
            "channelB_gain": 1,
            "channelA_offset": 53,
            "channelB_offset": 38,
            #         "clock_source": SpDigitiser.INTERNAL_CLOCK_SOURCE_INTERNAL_10MHZ_REFFERENCE,
            "clock_source": SpDigitiser.INTERNAL_CLOCK_SOURCE_EXTERNAL_10MHZ_REFFERENCE,
        },
        run_name=run_name,
        chA_background=chA_background,
        chB_background=chB_background,
    )

    plt.savefig(f"./{pp.STORE_FOLDER}/{run_name}.pdf")
    plt.show()


def g1_measurement(
    tau_points: int,
    no_runs: int,
    run_name: str,
    chA_background=None,
    chB_background=None,
    recompile=False,
):
    pp = G1Pipeline(ipython=False, TAU_POINTS=tau_points, recompile=recompile)
    plt.draw()  # Non-blocking drawing
    plt.pause(0.001)

    pp.execute_run(
        NO_RUNS=no_runs,
        digitiser_parameters={
            "delay": 0,
            "trigger_type": SpDigitiser.TRIGGER_EXTERNAL,
            #     "trigger_type": SpDigitiser.TRIGGER_SOFTWARE,
            "channelA_gain": 1,
            "channelB_gain": 1,
            "channelA_offset": 53,
            "channelB_offset": 38,
            #         "clock_source": SpDigitiser.INTERNAL_CLOCK_SOURCE_INTERNAL_10MHZ_REFFERENCE,
            "clock_source": SpDigitiser.INTERNAL_CLOCK_SOURCE_EXTERNAL_10MHZ_REFFERENCE,
        },
        run_name=run_name,
    )
    plt.savefig(f"./{pp.STORE_FOLDER}/{run_name}.pdf")
    plt.show()


if __name__ == "__main__":
    chA_background = None
    chB_background = None
    (chA_background, chB_background) = file_ops.load_chA_chB_arrays(
        "./store/sp400_background.csv"
    )

    # power_measurement(
    #     sp_points=400,
    #     no_runs=500_000,
    #     run_name="power-test",
    #     chA_background=chA_background,
    #     chB_background=chB_background,
    #     # recompile=True,
    # )

    g1_measurement(
        tau_points=1000,
        no_runs=2000,
        run_name="g1-test",
        chA_background=chA_background,
        chB_background=chB_background,
        # recompile=True,
    )
