#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Exception.h>
#include <string>
#include <fftw3.h> // for all fttw related items

#include <cufft.h>
#include "utils.hpp"
#include "g1_kernel.hpp"

class G1KernelGpuTest : public CppUnit::TestFixture {

    // Macro for generating suite
    CPPUNIT_TEST_SUITE( G1KernelGpuTest );

    // Population with tests
    // CPPUNIT_TEST_EXCEPTION( 🐙, CppUnit::Exception );
    CPPUNIT_TEST( test_g1_kernel );
    CPPUNIT_TEST( test_g1_kernel_with_background );

    CPPUNIT_TEST_SUITE_END();
private:
    const int tau_points = 200;

    short chA_data[G1_DIGITISER_POINTS];
    short chB_data[G1_DIGITISER_POINTS];

    double chA_g1[G1_DIGITISER_POINTS];
    double chB_g1[G1_DIGITISER_POINTS];
    double sq_g1[G1_DIGITISER_POINTS];

    short chA_background[G1_DIGITISER_POINTS];
    short chB_background[G1_DIGITISER_POINTS];

public:
    void setUp() {
        // Auxillary arrays used to load in data from specific columns
        short *_aux_arr_1[2] = {chA_data, chB_data};
        load_arrays_from_file(_aux_arr_1, "./test/test_files/g1_in.txt", 2, G1_DIGITISER_POINTS);

        double *_aux_arr_2[3] = {chA_g1, chB_g1, sq_g1};
        load_arrays_from_file(_aux_arr_2, "./test/test_files/g1_out_biased.txt",
                              3, tau_points);
    }
    void tearDown() {
    }

    void test_g1_kernel() {
        for (int i(0); i < G1_DIGITISER_POINTS; i++) {
            chA_background[i] = 0;
            chB_background[i] = 0;
        }

        G1::GPU::g1_memory memory = G1::GPU::allocate_memory();
        G1::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, memory);

        // Create plans
        cufftHandle *plans_forward(0); cufftHandle *plans_backward(0);
        G1::GPU::g1_prepare_fftw_plan(plans_forward, plans_backward);

        G1::GPU::g1_kernel(chA_data, chB_data,
                           memory,
                           plans_forward, plans_backward);

        for (int tau(0); tau < tau_points; tau++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("CHA Error on tau=" + std::to_string(tau),
                                                 chA_g1[tau],
                                                 memory.cpu_out[CHAG1][tau],
                                                 0.001);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("CHB Error on tau=" + std::to_string(tau),
                                                 chB_g1[tau],
                                                 memory.cpu_out[CHBG1][tau],
                                                 0.001);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("SQ Error on tau=" + std::to_string(tau),
                                                 sq_g1[tau],
                                                 memory.cpu_out[SQG1][tau],
                                                 0.001);
        }
        G1::GPU::free_memory(memory);
    }
    void test_g1_kernel_with_background() {
        short *_aux_arr[2] = {chA_background, chB_background};
        load_arrays_from_file(_aux_arr, "./test/test_files/g1_background_in.txt",
                              2, G1_DIGITISER_POINTS);

        double *_aux_arr_2[3] = {chA_g1, chB_g1, sq_g1};
        load_arrays_from_file(_aux_arr_2, "./test/test_files/g1_background_out.txt",
                              3, tau_points);

        G1::GPU::g1_memory memory = G1::GPU::allocate_memory();
        G1::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, memory);

        // Create plans
        cufftHandle *plans_forward(0); cufftHandle *plans_backward(0);
        G1::GPU::g1_prepare_fftw_plan(plans_forward, plans_backward);

        G1::GPU::g1_kernel(chA_data, chB_data,
                           memory,
                           plans_forward, plans_backward);

        for (int tau(0); tau < tau_points; tau++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("CHA Error on tau=" + std::to_string(tau),
                                                 chA_g1[tau],
                                                 memory.cpu_out[CHAG1][tau],
                                                 0.001);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("CHB Error on tau=" + std::to_string(tau),
                                                 chB_g1[tau],
                                                 memory.cpu_out[CHBG1][tau],
                                                 0.001);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("SQ Error on tau=" + std::to_string(tau),
                                                 sq_g1[tau],
                                                 memory.cpu_out[SQG1][tau],
                                                 0.001);
        }
        G1::GPU::free_memory(memory);
    }
};
CPPUNIT_TEST_SUITE_REGISTRATION( G1KernelGpuTest );
