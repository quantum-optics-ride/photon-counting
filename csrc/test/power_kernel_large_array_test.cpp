#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Exception.h>

#include "power_kernel.hpp"
#include "utils.hpp"
#include <string>

class PowerKernelLargeArrayTest : public CppUnit::TestFixture {

    // Macro for generating suite
    CPPUNIT_TEST_SUITE( PowerKernelLargeArrayTest );

    // Population with tests
    // CPPUNIT_TEST_EXCEPTION( 🐙, CppUnit::Exception );
    CPPUNIT_TEST ( test_power_kernel_cpu );
    CPPUNIT_TEST ( test_power_kernel_gpu );

    CPPUNIT_TEST_SUITE_END();
private:
    static const int sp_points = 400;
    static const int r_points = 32768;

    short chA_data[sp_points * r_points];
    short chB_data[sp_points * r_points];
    double expected_A_out[sp_points];
    double expected_B_out[sp_points];
    double expected_sq_out[sp_points];

    short chA_background[sp_points];
    short chB_background[sp_points];

    double *data_out[POWER::no_outputs];

public:
    void setUp(){
        double *_aux_arr_1[3] = {expected_A_out, expected_B_out, expected_sq_out};
        load_arrays_from_file(_aux_arr_1, "./test/test_files/power_in.txt", 3, sp_points);

        // This test data has the same 400 points repeated multiple times
        for (int sp(0); sp < sp_points; sp++) {
            for (int r(0); r < r_points; r++) {
                chA_data[r * sp_points + sp] = (short)expected_A_out[sp];
                chB_data[r * sp_points + sp] = (short)expected_B_out[sp];
            }
            chA_background[sp] = 0;
            chB_background[sp] = 0;
        }

        for (int i(0); i < POWER::no_outputs; i++)
            data_out[i] = new double[sp_points]();
    }
    void tearDown(){
        for (int i(0); i < POWER::no_outputs; i++)
            delete[] data_out[i];
    }
    void test_power_kernel_cpu() {
        int no_threads = 4;

        POWER::CPU::power_kernel(
            chA_data, chB_data, data_out,
            chA_background, chB_background,
            sp_points, r_points, no_threads);

        for (int sp(0); sp < sp_points; sp++) {
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHA sp=" + std::to_string(sp),
                                         expected_A_out[sp], data_out[CHA][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHB sp=" + std::to_string(sp),
                                         expected_B_out[sp], data_out[CHB][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("SQ sp=" + std::to_string(sp),
                                         expected_sq_out[sp], data_out[SQ][sp]);
        }
    }

    void test_power_kernel_gpu() {

        POWER::GPU::power_memory<double> memory = POWER::GPU::allocate_memory<double>(sp_points, r_points);
        POWER::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, sp_points);

        POWER::GPU::power_kernel(sp_points, r_points, (double)r_points,
                                    chA_data, chB_data,
                                    memory);

        for (int sp(0); sp < sp_points; sp++) {
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHA sp=" + std::to_string(sp),
                                         expected_A_out[sp], memory.data_out[CHA][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHB sp=" + std::to_string(sp),
                                         expected_B_out[sp], memory.data_out[CHB][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("SQ sp=" + std::to_string(sp),
                                         expected_sq_out[sp], memory.data_out[SQ][sp]);
        }

        POWER::GPU::free_memory(memory);
    }
};
CPPUNIT_TEST_SUITE_REGISTRATION( PowerKernelLargeArrayTest );
