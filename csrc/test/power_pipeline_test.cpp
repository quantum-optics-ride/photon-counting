#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Exception.h>
#include <stdexcept> //for std::runtime_error
#include <limits.h> // For LONG_MAX

#include "power_pipeline.hpp"
#include "sp_digitiser.hpp"
#include "ADQAPI.h" // For MultiRecordSetup

class PowerPipelineTest : public CppUnit::TestFixture {

    // Macro for generating suite
    CPPUNIT_TEST_SUITE( PowerPipelineTest );

    // Population with tests
    // CPPUNIT_TEST_EXCEPTION( 🐙, CppUnit::Exception );
    CPPUNIT_TEST( test_power_pipeline );
    CPPUNIT_TEST( test_power_pipeline_2_cycles );
    CPPUNIT_TEST( test_power_pipeline_too_many_repetitions );

    CPPUNIT_TEST_SUITE_END();

public:
    void* adq_cu_ptr;
    // Large number of points requested, so that there a fewer repetitions == fewer triggers -> faster test
    static const unsigned int sp_points = 10000;
    short chA_background[sp_points], chB_background[sp_points];

    void setUp(){
        adq_cu_ptr = master_setup(
            NO_BLINK,
            INTERNAL_CLOCK_SOURCE_INTERNAL_10MHZ_REFFERENCE,
            TRIGGER_EXTERNAL
            // TRIGGER_SOFTWARE
            );
    }
    void tearDown(){
        DeleteADQControlUnit(adq_cu_ptr);
    }

    void test_request_r_points() {
        unsigned int r_points = get_r_points_requested_from_digitiser(adq_cu_ptr, 10000, 1000);
        CPPUNIT_ASSERT_EQUAL((unsigned int)7483, r_points);

        r_points = get_r_points_requested_from_digitiser(adq_cu_ptr, 10000, 100);
        CPPUNIT_ASSERT_EQUAL((unsigned int)100, r_points);
    }

    void test_power_pipeline() {
        check_power_kernel_parameters();
        run_power_measurements(adq_cu_ptr,
                               chA_background, chB_background,
                               sp_points, 10, "./dump/power-pipeline-example.txt");
    };

    void test_power_pipeline_2_cycles() {
        check_power_kernel_parameters();
        // 10000 points will mean the digitiser does up to 7483 repetions.
        // Requesting more will thus probe the digitiser more that once, in order to test thread functionality.
        run_power_measurements(adq_cu_ptr,
                               chA_background, chB_background,
                               sp_points, 7483 + 2, "./dump/power-pipeline-example.txt");
    };

    void test_power_pipeline_too_many_repetitions() {
        check_power_kernel_parameters();
        CPPUNIT_ASSERT_THROW_MESSAGE(
            "Should give warning message that LONG arrays will overflow",
            run_power_measurements(adq_cu_ptr,
                                   chA_background, chB_background,
                                   sp_points, LONG_MAX, "./dump/power-pipeline-example.txt"),
            std::runtime_error);
    };
};
CPPUNIT_TEST_SUITE_REGISTRATION( PowerPipelineTest );
