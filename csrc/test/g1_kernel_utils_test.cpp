#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Exception.h>
#include <cmath> // for absolute

#include "g1_kernel.hpp"
#include "utils.hpp"

class G1KernelUtilsTest : public CppUnit::TestFixture {

    // Macro for generating suite
    CPPUNIT_TEST_SUITE( G1KernelUtilsTest );

    // Population with tests
    // CPPUNIT_TEST_EXCEPTION( 🐙, CppUnit::Exception );
    CPPUNIT_TEST( test_on_cpu );
    CPPUNIT_TEST( test_on_gpu_no_background );
    CPPUNIT_TEST( test_on_gpu_background );

    CPPUNIT_TEST_SUITE_END();

private:
    short chA_data[G1_DIGITISER_POINTS];
    short chB_data[G1_DIGITISER_POINTS];

    double expected_mean[3] = {
        7.950247483262897,
        -4.430522148000995,
        44702389.40396311
    };
    double expected_variance[3] = {
        22355718.64064403,
        22346587.927357536,
        799664705005884.9
    };

    // Background normalisation (only on GPU) /////////////////////////////////
    short chA_background[G1_DIGITISER_POINTS];
    short chB_background[G1_DIGITISER_POINTS];

public:
    void setUp(){
        short *_aux_arr_1[2] = {chA_data, chB_data};
        load_arrays_from_file(_aux_arr_1, "./test/test_files/g1_in.txt", 2, G1_DIGITISER_POINTS);
    }
    void tearDown(){
    }

    void test_on_cpu(){

        double mean_list[G1::no_outputs];
        double variance_list[G1::no_outputs];
        double chA_normalised[G1_DIGITISER_POINTS]; double chB_normalised[G1_DIGITISER_POINTS]; double sq_normalised[G1_DIGITISER_POINTS];
        double *normalised_data[G1::no_outputs];
        normalised_data[CHAG1] = chA_normalised;
        normalised_data[CHBG1] = chB_normalised;
        normalised_data[SQG1] = sq_normalised;

        G1::CPU::preprocessor(chA_data, chB_data,
                              G1_DIGITISER_POINTS,
                              mean_list, variance_list,
                              normalised_data);

        // 1% tolerance
        for (int i(0); i < 3; i++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                "Mean", expected_mean[i], mean_list[i],
                std::abs(0.01 * mean_list[i]));
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                "Variance", expected_variance[i], variance_list[i],
                0.01 * expected_variance[i]);
        }
        for (int i(0); i < 100; i++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Normalisation", chA_data[i] - expected_mean[CHAG1], normalised_data[CHAG1][i], 0.01);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                    "Normalisation", (double)chA_data[i] * chA_data[i] + chB_data[i] * chB_data[i] - expected_mean[SQG1],
                    normalised_data[SQG1][i], 0.01 * expected_mean[SQG1]);
        }
    }

    void test_on_gpu_no_background() {
        for (int i(0); i < G1_DIGITISER_POINTS; i++) {
            chA_background[i] = 0;
            chB_background[i] = 0;
        }

        float mean_list[G1::no_outputs]; float variance_list[G1::no_outputs];
        float chA_normalised[G1_DIGITISER_POINTS]; float chB_normalised[G1_DIGITISER_POINTS]; float sq_normalised[G1_DIGITISER_POINTS];
        float *normalised_data[G1::no_outputs];
        normalised_data[CHAG1] = chA_normalised;
        normalised_data[CHBG1] = chB_normalised;
        normalised_data[SQG1] = sq_normalised;

        G1::GPU::g1_memory memory = G1::GPU::allocate_memory();
        G1::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, memory);

        G1::check_g1_kernel_parameters(false);

        G1::GPU::preprocessor(
            G1_DIGITISER_POINTS, chA_data, chB_data,
            memory.gpu_raw_data, memory.gpu_background, reinterpret_cast<float**>(memory.gpu_inout),
            memory.gpu_pp_aux, memory.gpu_mean, memory.gpu_variance,
            mean_list, variance_list, normalised_data);

        // 1% tolerance
        for (int i(0); i < 3; i++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                "Mean", expected_mean[i], mean_list[i],
                std::abs(0.01 * mean_list[i]));
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                "Variance", expected_variance[i], variance_list[i],
                0.01 * expected_variance[i]);
        }
        for (int i(0); i < 100; i++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Normalisation", chA_data[i] - expected_mean[CHAG1], normalised_data[CHAG1][i], 0.01);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                "Normalisation", (double)chA_data[i] * chA_data[i] + chB_data[i] * chB_data[i] - expected_mean[SQG1],
                normalised_data[SQG1][i], 0.01 * expected_mean[SQG1]);
        }

        G1::GPU::free_memory(memory);
    }

    void test_on_gpu_background() {
        short *_aux_arr[2] = {chA_background, chB_background};
        load_arrays_from_file(_aux_arr, "./test/test_files/g1_background_in.txt",
                              2, G1_DIGITISER_POINTS);

        float mean_list[G1::no_outputs]; float variance_list[G1::no_outputs];
        float chA_normalised[G1_DIGITISER_POINTS]; float chB_normalised[G1_DIGITISER_POINTS]; float sq_normalised[G1_DIGITISER_POINTS];
        float *normalised_data[G1::no_outputs] = {chA_normalised, chB_normalised, sq_normalised};

        G1::GPU::g1_memory memory = G1::GPU::allocate_memory();
        G1::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, memory);

        G1::check_g1_kernel_parameters(false);

        G1::GPU::preprocessor(
            G1_DIGITISER_POINTS, chA_data, chB_data,
            memory.gpu_raw_data, memory.gpu_background, reinterpret_cast<float**>(memory.gpu_inout),
            memory.gpu_pp_aux, memory.gpu_mean, memory.gpu_variance,
            mean_list, variance_list, normalised_data);

        double expected_mean_with_bg[3] = {
            7.020112522443994,
            6.051075955884303,
            89478083.455586
        };
        double expected_variance_with_bg[3] = {
            44688903.084844984,
            44789094.473240964,
            5587876214702662.0
        };

        for (int i(0); i < 2; i++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Mean",
                                                 expected_mean_with_bg[i], mean_list[i],
                                                 std::abs(0.01 * mean_list[i]));
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE( "Variance",
                                                  expected_variance_with_bg[i], variance_list[i],
                                                  0.01 * expected_variance_with_bg[i]);
        }
        for (int i(0); i < 100; i++) {
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Normalisation",
                                                 chA_data[i] - expected_mean_with_bg[CHAG1] - chA_background[i], normalised_data[CHAG1][i], 0.01);
            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE("Normalisation",
                                                 chB_data[i] - expected_mean_with_bg[CHBG1] - chB_background[i], normalised_data[CHBG1][i], 0.01);
        }

        G1::GPU::free_memory(memory);
    }
};
CPPUNIT_TEST_SUITE_REGISTRATION( G1KernelUtilsTest );
