#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Exception.h>
#include <stdexcept> //for std::runtime_error
#include <limits.h> // For LONG_MAX

#include "g1_pipeline.hpp"
#include "sp_digitiser.hpp"
#include "ADQAPI.h" // For MultiRecordSetup

class G1PipelineTest : public CppUnit::TestFixture {

    // Macro for generating suite
    CPPUNIT_TEST_SUITE( G1PipelineTest );

    // Population with tests
    // CPPUNIT_TEST_EXCEPTION( 🐙, CppUnit::Exception );
    CPPUNIT_TEST( test_g1_pipeline );

    CPPUNIT_TEST_SUITE_END();

public:
    void* adq_cu_ptr;
    short chA_background[G1_DIGITISER_POINTS];
    short chB_background[G1_DIGITISER_POINTS];

    void setUp(){
        adq_cu_ptr = master_setup(
            NO_BLINK,
            INTERNAL_CLOCK_SOURCE_INTERNAL_10MHZ_REFFERENCE,
            TRIGGER_SOFTWARE);
    }
    void tearDown(){
        DeleteADQControlUnit(adq_cu_ptr);
    }

    void test_g1_pipeline() {

        run_g1_measurements(adq_cu_ptr,
                            chA_background, chB_background,
                            10, "./dump/g1-pipeline-example.txt",
                            100
            );

        ADQ214_MultiRecordClose(adq_cu_ptr, 1);
    };
};
CPPUNIT_TEST_SUITE_REGISTRATION( G1PipelineTest );
