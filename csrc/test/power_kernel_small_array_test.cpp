#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Exception.h>

#include "power_kernel.hpp"
#include "utils.hpp"
#include <string>

class PowerKernelShortTest : public CppUnit::TestFixture {

    // Macro for generating suite
    CPPUNIT_TEST_SUITE( PowerKernelShortTest );

    // Population with tests
    // CPPUNIT_TEST_EXCEPTION( 🐙, CppUnit::Exception );
    CPPUNIT_TEST ( test_power_kernel_cpu );
    CPPUNIT_TEST ( test_power_kernel_gpu );
    CPPUNIT_TEST ( test_power_kernel_cumulative_gpu );

    CPPUNIT_TEST_SUITE_END();
private:
    static const int sp_points = 3;
    static const int r_points = 8;

    short chA_data[sp_points * r_points] = {2, 2, 3,
                                            1, 2, 3,
                                            1, 2, 3,
                                            1, 2, 3,
                                            3, 3, 4,
                                            5, 5, 5,
                                            4, 3, 4,
                                            4, 3, 11};
    short chB_data[sp_points * r_points] = {0, 1, 2,
                                            3, 4, 5,
                                            6, 7, 9,
                                            10, 11, 12,
                                            0, 0, 0,
                                            1, 1, 1,
                                            2, 2, 2,
                                            3, 3, 3};
    short chA_background[sp_points] = {1, 2, 3};
    short chB_background[sp_points] = {0, 0, 0};

    double expected_A_out[sp_points] = {1.625, 0.75, 1.5};
    double expected_B_out[sp_points] = {3.125, 3.625, 4.25};
    double expected_sq_out[sp_points] = {24.75, 26.625, 42.25};

    double *data_out[POWER::no_outputs];

public:
    void setUp(){
        for (int i(0); i < POWER::no_outputs; i++)
            data_out[i] = new double[sp_points]();
    }
    void tearDown(){
        for (int i(0); i < POWER::no_outputs; i++)
            delete[] data_out[i];
    }
    void test_power_kernel_cpu() {
        int no_threads = 2;

        POWER::CPU::power_kernel(
            chA_data, chB_data, data_out,
            chA_background, chB_background,
            sp_points, r_points, no_threads);

        for (int sp(0); sp < sp_points; sp++) {
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHA sp=" + std::to_string(sp),
                                         expected_A_out[sp], data_out[CHA][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHB sp=" + std::to_string(sp),
                                         expected_B_out[sp], data_out[CHB][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("SQ sp=" + std::to_string(sp),
                                         expected_sq_out[sp], data_out[SQ][sp]);
        }
    }
    void test_power_kernel_gpu() {

        POWER::GPU::power_memory<double> memory = POWER::GPU::allocate_memory<double>(sp_points, r_points);
        POWER::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, sp_points);

        POWER::GPU::power_kernel(sp_points, r_points, (double)r_points,
                                    chA_data, chB_data,
                                    memory);

        for (int sp(0); sp < sp_points; sp++) {
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHA sp=" + std::to_string(sp),
                                         expected_A_out[sp], memory.data_out[CHA][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHB sp=" + std::to_string(sp),
                                         expected_B_out[sp], memory.data_out[CHB][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("SQ sp=" + std::to_string(sp),
                                         expected_sq_out[sp], memory.data_out[SQ][sp]);
        }

        POWER::GPU::free_memory(memory);
    }

    void test_power_kernel_cumulative_gpu() {

        POWER::GPU::power_memory<long> memory = POWER::GPU::allocate_memory<long>(sp_points, r_points);
        POWER::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, sp_points);

        POWER::GPU::power_kernel(sp_points, r_points, (long)1,
                                    chA_data, chB_data,
                                    memory);

        for (int sp(0); sp < sp_points; sp++) {
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHA sp=" + std::to_string(sp),
                                         (long)(r_points * expected_A_out[sp]), memory.data_out[CHA][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("CHB sp=" + std::to_string(sp),
                                         (long)(r_points * expected_B_out[sp]), memory.data_out[CHB][sp]);
            CPPUNIT_ASSERT_EQUAL_MESSAGE("SQ sp=" + std::to_string(sp),
                                         (long)(r_points * expected_sq_out[sp]), memory.data_out[SQ][sp]);
        }

        POWER::GPU::free_memory(memory);
    }
};
CPPUNIT_TEST_SUITE_REGISTRATION( PowerKernelShortTest );
