#ifndef G1_PIPELINE_HPP
#define G1_PIPELINE_HPP

#include <string>
#include <cufft.h> // for cufftHandle

#include "g1_kernel.hpp"

#ifdef __cplusplus
extern "C" {
#endif
    /**
     * Run pipeline for g1 measurements using 2 threads parallel that alternate between:
     * - Read in from digitiser
     * - Evaluate CHAG1, CHBG1, SQG1 and dumping to file
     *
     * Ensure that:
     * - Background data has been copied over to the GPU
     * - `MultiRecordSetup` has been run to prepare the digitizer for measurements.
     * - `MultiRecordClose` is run after the function to reset digitiser.
     *
     * @param adq_cu_ptr Allocated pointer for communication with digitizer.
     * @param chA_background, chB_background of length G1_DIGITISER_POINTS each. These will be subtracted from the channels before processing begins.
     * @param no_runs Number of times to repeat the measurements.
     * @param base_filename File to which to dump the results, using log-rotation format `base_filename_X.txt`
     * @param tau_points Number of points in time to evaluate correlation for
     * @returns 0 for success.
     */
    int run_g1_measurements(void* adq_cu_ptr,
                            short* chA_background, short* chB_background,
                            unsigned long no_runs, char* base_filename, int tau_points);
#ifdef __cplusplus
}
#endif

// Unit testing and bechmarking
#ifdef BENCH
void process_g1_digitiser_data(
    short *chA_data, short *chB_data,
    G1::GPU::g1_memory memory,
    cufftHandle *plans_forward, cufftHandle *plans_backward,
    std::string base_filename,
    float **cumulative, unsigned long run, int tau_points);
#endif

#endif
