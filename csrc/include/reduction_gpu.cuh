/**
 * Generic tools for performing reduction (summation) on the GPU.
 */

#ifndef REDUCTION_GPU_HPP
#define REDUCTION_GPU_HPP

#include "utils_gpu.hpp"

#define FULL_MASK 0xffffffff

/**
 * Instead of delcaring shared memory like this:
 *
 *     extern __shared__ T shared[]; // Size of shared array depends on parameter passed in at kernel launch.
 *
 * this is done with an auxillary class used to avoid linker errors with extern
 * unsized shared memory arrays with templated type.
 */
template<typename T>
struct SharedMemory
{
    __device__ inline operator       T *()
        {
            extern __shared__ int __smem[];
            return (T *)__smem;
        }

    __device__ inline operator const T *() const
        {
            extern __shared__ int __smem[];
            return (T *)__smem;
        }
};

/**
 * Specialize for double to avoid unaligned memory access compile errors
 */
template<>
struct SharedMemory<double>
{
    __device__ inline operator       double *()
        {
            extern __shared__ double __smem_d[];
            return (double *)__smem_d;
        }

    __device__ inline operator const double *() const
        {
            extern __shared__ double __smem_d[];
            return (double *)__smem_d;
        }
};

/**
 * The 32 threads within a single warp are reduced to the first lane (thread0), by succesively copying from threads at a given offset
 * See https://developer.nvidia.com/blog/faster-parallel-reductions-kepler/ for diagram of procedure.
 *
 * Here `val` was the original value held by the thread.
 * - Threads with higher indicies have their values successively added onto this thread's value.
 * - By supplying `val` in the `__shfl_down_sync` command, this threads value is exposed for shuffling to other threads as well.
 */
template <typename T>
__inline__ __device__
T warp_reduce_sum(T val) {
    for (int offset = WARP_SIZE/2; offset > 0; offset /= 2)
        val += __shfl_down_sync(FULL_MASK, val, offset);
    return val;
}

/**
 * Whereas `warp_reduce_sum` reduces values in threads running in a single warp (32 threads)
 * this function recues all 1024 threads (32 warps) within a single block
 */
template <typename T>
__inline__ __device__
T block_reduce_sum(T val) {
    T *shared = SharedMemory<T>();

    unsigned int wid = threadIdx.x / WARP_SIZE; // Warp index
    unsigned int lane = threadIdx.x % WARP_SIZE; // Lane that the current thread occupies in that warp

    // Reduce the 32 threads in each warp (into lane 0) and store it in shared memory under the relevant warp index.
    val = warp_reduce_sum<T>(val);
    if (lane == 0) shared[wid] = val;
    // Await all threads to complete reduction
    __syncthreads();

    // At this stage the shared array is `[warp0sum, warp1sum, warp2sum, ..., (Number of threads / warpsize)sum]`
    // which need to be reduced again (into lane 0).
    // First check if the thread will be involved in this reduction (assign 0 otherwise)
    val = (threadIdx.x < blockDim.x / WARP_SIZE) ? shared[lane] : 0;

    // Only need to utilise threads from the first warp for this final summation
    // ! This assumes that one warp (32 threads) will be larger enough for the shared array size.
    if (wid == 0) val = warp_reduce_sum<T>(val);

    // Await all threads to complete. This is because subsequent calls to this kernel could launch reductions with threads still running with values from the previous run
    __syncthreads();
    return val;
}

#endif
