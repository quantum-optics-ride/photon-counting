#ifndef POWER_PIPELINE_HPP
#define POWER_PIPELINE_HPP

#include <string>

#include "power_kernel.hpp"

#ifdef __cplusplus
extern "C" {
#endif
    /**
     * Run pipeline for power measurements using 2 threads parallel that alternate between:
     * - Read in from digitiser
     * - Evaluation CHA, CHB, CHASQ, CHBSQ, SQ and dumping to file
     *
     * Ensure that:
     * - Background data has been copied over to the GPU
     * - `MultiRecordSetup` has been run to prepare the digitizer for measurements.
     * - `MultiRecordClose` is run after the function to reset digitiser.
     *
     * @param adq_cu_ptr Allocated pointer for communication with digitizer.
     * @param chA_background, chB_background of length SP_POINTS each. These will be subtracted from the channels before processing begins.
     * @param sp_points Number of values to read. 100 points == 250ns
     * @param no_runs Number of times to repeat the measurements.
     * @param base_filename File to which to dump the results, using log-rotation format `base_filename_X.txt`
     * @returns 0 for success.
     */
    int run_power_measurements(void* adq_cu_ptr,
                               short *chA_background, short *chB_background,
                               unsigned int sp_points, unsigned long no_runs, char* base_filename);

    /**
     * Evaluate the number of records that will be requested from the digitiser:
     * - `sp_points` limits the maximum number - the digitiser only has a certain memory capacity
     * - `no_runs` limits the minimum number - if only 1 run is requested, then one 1 record will be read from digitiser.
     */
    unsigned int get_r_points_requested_from_digitiser(void *adq_cu_ptr,
                                                       unsigned int sp_points, unsigned int no_runs);

#ifdef __cplusplus
}
#endif

// Unit testing and bechmarking
#ifdef BENCH
void process_power_digitiser_data(
    unsigned int sp_points, unsigned int r_points,
    short *chA_data, short *chB_data,
    POWER::GPU::power_memory<long> memory,
    std::string base_filename, long **cumulative, unsigned long run);
#endif

#endif
