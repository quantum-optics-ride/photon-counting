#ifndef POWER_KERNEL_HPP
#define POWER_KERNEL_HPP

#include <cmath>
#include "utils_gpu.hpp"

#ifndef SP_POINTS
#define SP_POINTS 200 ///< Number of time points (sampk points) read from digitiser
#endif

// Verbose Indexes used for accessing array elements in a human-readable way e.g. array[CHASQ]
#define CHA 0
#define CHB 1
#define CHASQ 2
#define CHBSQ 3
#define SQ 4

#ifdef __cplusplus
extern "C" {
#endif

    /**
     * Validation of kernel parameters before it's invocation.
     */
    int check_power_kernel_parameters(bool display=false);

#ifdef __cplusplus
}
#endif

/**
 * @brief \f$ \left\langle{chA}\right\rangle, \left\langle{chB}\right\rangle, \left\langle{chA^2}\right\rangle, \left\langle{chB^2}\right\rangle, \left\langle{chA^2 + chB^2}\right\rangle \f$ measurements.
 *
 * We expect the digitiser to return (samples per record) repeated (number of records).
 * Therefore the chA and chB sizes are `sp_points * r_points`, which are processed to give an average value:
 * - \f[ \left\langle{chA}\right\rangle \f]
 * - \f[ \left\langle{chB}\right\rangle \f]
 * - \f[ \left\langle{chA^2}\right\rangle \f]
 * - \f[ \left\langle{chB^2}\right\rangle \f]
 * - \f[ \left\langle{chA^2 + chB^2}\right\rangle \f]
 */
namespace POWER {

    const int no_outputs = 5; ///< Kernels returns `[CHA CHB CHASQ CHBSQ SQ]`

    namespace CPU {

        void power_kernel(
            short *chA_data,
            short *chB_data,
            double** data_out,
            short *chA_back,
            short *chB_back,
            int sp_points,
            int r_points,
            int number_of_threads
            );
    }

    namespace GPU {

        const int threads = 1024; ///< Threads used in evaluation. More is better, but since they use shared memory (see get_shared_memory_size) we should restrict them.
        const int no_outputs_from_gpu = 4; ///< GPU will be computing this number of results.

        /**
         * Input data is split into multiple interleaved reduction blocks in order to stay within shared memory limits on GPU:
         * e.g. sp=0 has 4000 repetitions and 1024 threads have been launched
         * Thus there will be ceil(4000/1024) = 4 blocks that will be reduced individually.
         */
        unsigned int get_number_of_reduction_blocks(unsigned int r_points);

        /**
         * Shared memory is allocated so that each thread has a unique location to write/read results from.
         */
        template <typename T> unsigned int get_shared_memory_size();

        /**
         * Copying of background data once into constant memory on the GPU - this will be subtracted from all input data before processing.
         *
         * @param chA_background, chB_background arrays to copy over to GPU.
         * @param N length of arrays. Ideally `N=SP_POINTS` so that just enough memory if allocated for copying them over.
         */
        void copy_background_arrays_to_gpu(short *chA_background, short *chB_background, int N=SP_POINTS);

        /**
         * @brief Collection of memory allocations for power evaluation on the GPU.
         *
         * @tparam T double or long
         */
        template <typename T>
        struct power_memory {
            short *gpu_in[2]; ///< `[CHA, CHB]` input arrays on the gpu
            T *data_out[POWER::no_outputs]; ///< `[CHA, CHB, CHASQ, CHBSQ, SQ]` arrays on cpu where results are written to
            T *gpu_aux[POWER::no_outputs]; ///< Used in intermediate execution
        };
        template <typename T>
        power_memory<T> allocate_memory(unsigned int sp_points, unsigned int r_points);
        template <typename T>
        void free_memory(power_memory<T> memory);

        template<typename T>
        void power_kernel(
            unsigned int sp_points, unsigned int r_points, T normalisation,
            short *chA_data, short *chB_data,
            power_memory<T> memory);
    }
}

#endif
