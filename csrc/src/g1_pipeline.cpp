#include <thread> // for std::thread
#include "ADQAPI.h" // For MultiRecordSetup and MultiRecordClose
#include <string>

#include "logging.hpp"
#include "sp_digitiser.hpp"
#include "g1_kernel.hpp"
#include "g1_pipeline.hpp"
#include "utils.hpp"

/**
 * @brief Process data from digitiser, accumulate, normalise and dump to file.
 *
 * @param chA_data, chB_data data from the digitiser.
 * @param memory allocated using G1::GPU::allocate_memory
 * @param cumulative cumulative array which will be incremented every iteration
 * @param run cumulative data is normalised by the number runs completed before dumping to file
 * @param base_filename Data is dumped to a file using log rotation format.
 */
void process_g1_digitiser_data(
    short *chA_data, short *chB_data,
    G1::GPU::g1_memory memory,
    cufftHandle *plans_forward, cufftHandle *plans_backward,
    std::string base_filename, float **cumulative, unsigned long run, int tau_points
    ){
    G1::GPU::g1_kernel(
        chA_data, chB_data,
        memory,
        plans_forward, plans_backward);

    dump_arrays_to_file(
        memory.cpu_out, cumulative,
        G1::no_outputs,
        tau_points,
        base_filename + std::to_string(run % LOG_ROTATE) + ".csv",
        "# Run " + std::to_string(run) +  "\n# CHAG1\tCHBG1\tSQG1",
        (double)run
        );
};

int run_g1_measurements(void* adq_cu_ptr,
                        short* chA_background, short* chB_background,
                        unsigned long no_runs, char* base_filename, int tau_points){
    const int no_threads = 2;

    PYTHON_START;

    G1::check_g1_kernel_parameters(false);

    cufftHandle *plans_forward(0); cufftHandle *plans_backward(0);
    G1::GPU::g1_prepare_fftw_plan(plans_forward, plans_backward);

    // Allocation of memory and copying of background data
    G1::GPU::g1_memory memory = G1::GPU::allocate_memory();
    G1::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, memory);

    // There will be 2 copies of chA_data and chB_data.
    // One thread can be reading into one pair (chA, chB),
    // Other thread will be evaluating the correlatio (chA, chB)
    short chA_data_0[G1_DIGITISER_POINTS], chA_data_1[G1_DIGITISER_POINTS]; short *chA_data[no_threads] = {chA_data_0, chA_data_1};
    short chB_data_0[G1_DIGITISER_POINTS], chB_data_1[G1_DIGITISER_POINTS]; short *chB_data[no_threads] = {chB_data_0, chB_data_1};
    float *cumulative[G1::no_outputs];
    for (int i(0); i < G1::no_outputs; i++)
        cumulative[i] = new float[G1_DIGITISER_POINTS]();

    // 2. Prepare for multirecord mode
    ADQ214_MultiRecordSetup(adq_cu_ptr, 1, 1, G1_DIGITISER_POINTS);

    // 4. Launch 2 parrallel threads, alternating between fetching from digitiser and processing on GPU.
    std::thread thread_list[no_threads];
    int dth(0), pth(1); // flip-floppers between 0 and 1. DigitizerTHread and ProcessingTHread

    // Initial read into digitiser
    fetch_digitiser_data(adq_cu_ptr, chA_data[dth], chB_data[dth], G1_DIGITISER_POINTS, 1);

    for (unsigned long r(1); r < no_runs; r++) {
        // XOR to switch 0 <-> 1
        dth ^= 1; pth ^= 1;

        thread_list[0] = std::thread(fetch_digitiser_data,
                                     adq_cu_ptr,
                                     chA_data[dth], chB_data[dth],
                                     G1_DIGITISER_POINTS, 1);
        thread_list[1] = std::thread(process_g1_digitiser_data,
                                     chA_data[pth], chB_data[pth],
                                     memory,
                                     plans_forward, plans_backward,
                                     base_filename,
                                     cumulative, r, tau_points);
        thread_list[0].join();
        thread_list[1].join();
    }
    dth ^= 1; pth ^= 1;
    // Final processing of digitiser data
    process_g1_digitiser_data(chA_data[pth], chB_data[pth],
                              memory,
                              plans_forward, plans_backward,
                              base_filename,
                              cumulative, no_runs, tau_points);

    // Reset
    G1::GPU::free_plan(plans_forward, plans_backward);
    G1::GPU::free_memory(memory);
    for (int i(0); i < G1::no_outputs; i++)
        delete[] cumulative[i];
    ADQ214_MultiRecordClose(adq_cu_ptr, 1);

    PYTHON_END;
    return 0;
}
