#include <stdexcept> //for std::runtime_error
#include <string> // for std::to_string
#include <thread> // for std::thread
#include <limits.h> // For LONG_MAX
#include "ADQAPI.h" // For MultiRecordSetup and MultiRecordClose
#include <cmath> // for std::ceil

#include "logging.hpp"
#include "utils.hpp"
#include "sp_digitiser.hpp"
#include "power_pipeline.hpp"
#include "power_kernel.hpp"

/**
 * @brief Process data from digitiser, accumulate, normalise and dump to file.
 *
 * @param chA_data, chB_data data from the digitiser.
 * @param memory allocated using POWER::GPU::allocate_memory
 * @param cumulative cumulative array which will be incremented every iteration
 * @param cycle cumulative data is normalised by the number runs completed before dumping to file
 * @param base_filename Data is dumped to a file using log rotation format.
 */
void process_power_digitiser_data(
    unsigned int sp_points, unsigned int r_points,
    short *chA_data, short *chB_data,
    POWER::GPU::power_memory<long> memory,
    std::string base_filename, long **cumulative, unsigned long cycle){

    POWER::GPU::power_kernel(
        sp_points, r_points, long(1),
        chA_data, chB_data,
        memory);

    dump_arrays_to_file(
        memory.data_out, cumulative,
        POWER::no_outputs, SP_POINTS,
        base_filename + std::to_string(cycle % LOG_ROTATE) + ".csv",
        "# Run " + std::to_string(MAX(cycle * r_points, r_points)) +  "\n# CHA\tCHB\tCHASQ\tCHBSQ\tSQ",
        (double)cycle * r_points
        );
};

unsigned int get_r_points_requested_from_digitiser(
    void *adq_cu_ptr,
    unsigned int sp_points, unsigned int no_runs) {
    return MIN(GetMaxNofRecordsFromNofSamples(adq_cu_ptr, sp_points), no_runs);
}

int run_power_measurements(void* adq_cu_ptr,
                           short* chA_background, short* chB_background,
                           unsigned int sp_points, unsigned long no_runs, char* base_filename){
    PYTHON_START;
    const int no_threads = 2; ///< One thread will be used to request data from digitiser. Other thread will process the data and store it in file
    const unsigned int r_points = get_r_points_requested_from_digitiser(adq_cu_ptr, sp_points, no_runs);
    const unsigned long no_cycles = std::ceil((float)no_runs / r_points);
    OKBLUE("Power Pipeline: Executing with\nSP_POINTS: %i\nR_POINTS:%i \nCycles: %i",
           sp_points, r_points, no_cycles
        );

    // Check valid amount of repetitions is used to prevent overflow
    // We cast to largest data type (unsigned long long) for comparisson
    unsigned long long max_cumulative_value = 2 * MAX_DIGITISER_CODE * MAX_DIGITISER_CODE * no_runs;
    if (max_cumulative_value > (unsigned long long)LONG_MAX)
        FAIL("No runs ("
             + std::to_string(no_runs)
             + ") x 14bit Code ("
             + std::to_string(MAX_DIGITISER_CODE)
             + ")^2 = "
             + std::to_string(max_cumulative_value)
             + "bytes > maximum size that cumulative arrays of type LONG can hold ("
             + std::to_string(LONG_MAX)
             + "bytes)");

    // 1. Allocation of memory
    // There will be 2 copies of chA_data and chB_data.
    // One thread can be reading into one pair (chA, chB),
    // Other thread can be processing the other pair (chA, chB)
    short *chA_data[no_threads];
    short *chB_data[no_threads];
    for (int t(0); t < no_threads; t++) {
        chA_data[t] = new short[sp_points * r_points];
        chB_data[t] = new short[sp_points * r_points];
    }
    long *cumulative_data[POWER::no_outputs];
    for (int i(0); i < POWER::no_outputs; i++)
        cumulative_data[i] = new long[SP_POINTS]();
    POWER::GPU::power_memory<long> memory = POWER::GPU::allocate_memory<long>(sp_points, r_points);

    // 2. Prepare for multirecord mode
    ADQ214_MultiRecordSetup(adq_cu_ptr, 1, r_points, sp_points);

    // 3. Copy background data onto GPU
    POWER::GPU::copy_background_arrays_to_gpu(chA_background, chB_background, sp_points);

    // 4. Launch 2 parrallel threads, alternating between fetching from digitiser and processing on GPU.
    std::thread thread_list[no_threads];
    int dth(0), pth(1); // flip-floppers between 0 and 1. DigitizerTHread and ProcessingTHread

    // Initial read into digitiser
    fetch_digitiser_data(adq_cu_ptr, chA_data[dth], chB_data[dth], sp_points, r_points);
    for (unsigned long c(1); c < no_cycles; c++) {
        // XOR to switch 0 <-> 1
        dth ^= 1; pth ^= 1;

        thread_list[0] = std::thread(fetch_digitiser_data,
                                     adq_cu_ptr,
                                     chA_data[dth], chB_data[dth],
                                     sp_points, r_points);

        thread_list[1] = std::thread(process_power_digitiser_data,
                                     sp_points, r_points,
                                     chA_data[pth], chB_data[pth],
                                     memory,
                                     base_filename, cumulative_data, c);
        thread_list[0].join();
        thread_list[1].join();
    }
    dth ^= 1; pth ^= 1;
    // Final processing of digitiser data
    process_power_digitiser_data(
        sp_points, r_points,
        chA_data[pth], chB_data[pth],
        memory,
        base_filename, cumulative_data, no_cycles);

    // Deallocation of memory
    for (int t(0); t < no_threads; t++) {
        delete[] chA_data[t];
        delete[] chB_data[t];
    }
    for (int i(0); i < POWER::no_outputs; i++)
        delete[] cumulative_data[i];
    POWER::GPU::free_memory(memory);

    // Resetting device
    ADQ214_MultiRecordClose(adq_cu_ptr, 1);

    PYTHON_END;
    return 0;
};
