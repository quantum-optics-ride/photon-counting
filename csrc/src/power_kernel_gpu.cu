#include <string>
#include <cmath>

#include "logging.hpp"
#include "power_kernel.hpp"
#include "reduction_gpu.cuh"

// Background signal copied once to GPU
__constant__ short gpu_chA_background[SP_POINTS];
__constant__ short gpu_chB_background[SP_POINTS];

void POWER::GPU::copy_background_arrays_to_gpu(short *chA_background, short *chB_background, int N) {
    if (N > SP_POINTS) FAIL("Background arrays ("
                            + std::to_string(N)
                            + ") larger than the maximum SP_POINTS("
                            + std::to_string(SP_POINTS)
                            + ") kernel was compiled with."
        );

    int success = 0;
    success += cudaMemcpyToSymbol(gpu_chA_background, chA_background,
                                  N*sizeof(short));
    success += cudaMemcpyToSymbol(gpu_chB_background, chB_background,
                                  N*sizeof(short));
    if (success != 0) FAIL("Failed to copy background data TO the GPU");
}

/**
 * In this first reduction sweep CHASQ, CHBSQ and SQ are evaluated prior to summing up.
 *
 * The input itself looks like:
 * R0-SP0       R0-SP1       R0-SP2       ... R0-SP_POINTS
 * R1-SP0       R1-SP1       R1-SP2       ... R1-SP_POINTS
 * R2-SP0       R2-SP1       R2-SP2       ... R2-SP_POINTS
 * ...          ...          ...          ... ....
 * r_points-SP0 r_points-SP1 r_points-SP2 ... r_points-SP_POINTS
 *
 * Which is partially reduced into blocks (each block sums up WARPSIZE=32 repetitions)
 *
 *     [
 *       block1sum-SP0, block1sum-SP1, block1sum-SP2, ..., block1sum-SP_POINTS,
 *       block2sum-SP0, block2sum-SP1, block2sum-SP2, ..., block2sum-SP_POINTS,
 *       ....
 *       r_points/NO_THREADSsum-SP0, ...
 *     ]
 *
 * that need to be reduced one more time.
 */
template <typename T>
__global__ void kernel_1(
    short *gpu_in_chA, short *gpu_in_chB,
    T *gpu_aux_chA, T *gpu_aux_chB, T *gpu_aux_chAsq, T *gpu_aux_chBsq,
    unsigned int sp_points, unsigned int r_points
    ) {

    T chA_sum, chB_sum, chAsq_sum, chBsq_sum;
    unsigned int N = r_points * sp_points;

    /** In general, r_points >> sp_points,
     * and so it is best to utilise the GPU
     * by splitting up reduction along the repetition axis:
     * - standard for loop across sp_points (sp)
     * - each block will deal with a section of repetions under for this sp value
     */

    // Traverse all the sp_points, performing a partial reduction for each one
    for (int sp = 0; sp < sp_points; sp++) {

        // Move to the target block + target repetition + target column
        unsigned int tidx = (blockIdx.x * blockDim.x * sp_points) + (threadIdx.x * sp_points) + sp;
        chA_sum = (tidx < N) ? (T)gpu_in_chA[tidx] - gpu_chA_background[sp] : 0;
        chB_sum = (tidx < N) ? (T)gpu_in_chB[tidx] - gpu_chB_background[sp] : 0;
        chAsq_sum = chA_sum * chA_sum;
        chBsq_sum = chB_sum * chB_sum;

        // Await all threads to finish initial assingment
        __syncthreads();

        // Perform reduction
        chA_sum = block_reduce_sum<T>(chA_sum);
        chB_sum = block_reduce_sum<T>(chB_sum);
        chAsq_sum = block_reduce_sum<T>(chAsq_sum);
        chBsq_sum = block_reduce_sum<T>(chBsq_sum);

        // With the value reduced in the first thread, store them for the next reduction kernel
        if (threadIdx.x == 0) {
            gpu_aux_chA[sp + blockIdx.x * sp_points] = chA_sum;
            gpu_aux_chB[sp + blockIdx.x * sp_points] = chB_sum;
            gpu_aux_chAsq[sp + blockIdx.x * sp_points] = chAsq_sum;
            gpu_aux_chBsq[sp + blockIdx.x * sp_points] = chBsq_sum;
        }
    }
}
/**
 * Summation of `gpu_in` into `gpu_out` with optional normalisation.
 *
 * `gpu_in` is an inverleaved array of partially reduced data:
 *
 *     [
 *       block1sum-SP0, block1sum-SP1, block1sum-SP2, ..., block1sum-SP_POINTS,
 *       block2sum-SP0, block2sum-SP1, block2sum-SP2, ..., block2sum-SP_POINTS,
 *       ....
 *       reduction_blocks_Ssum-SP0, ...
 *     ]
 *
 * This kernel performs the final reduction to get:
 *
 *     [sumSP0, sumSP1, sumSP2, ...]
 *
 */
template <typename T>
__global__ void kernel_2(
    T *gpu_in_chA,  T *gpu_in_chB, T *gpu_in_chAsq, T *gpu_in_chBsq,
    T *gpu_out_chA, T *gpu_out_chB, T *gpu_out_chAsq, T *gpu_out_chBsq, T *gpu_out_sq,
    unsigned int sp_points, unsigned int reduction_blocks, T normalisation) {

    T chA_sum, chB_sum, chAsq_sum, chBsq_sum;

    for (int sp = blockIdx.x; sp < sp_points; sp += gridDim.x) {
        chA_sum = chB_sum = chAsq_sum = chBsq_sum = 0;

        for (
            // Move to the reduction block + target column
            int tidx = (threadIdx.x * sp_points) + sp;
            tidx < sp_points * reduction_blocks;
            // Increment by the number of running threads to the next unhandled block
            tidx += blockDim.x * sp_points
            ) {
            chA_sum += gpu_in_chA[tidx];
            chB_sum += gpu_in_chB[tidx];
            chAsq_sum += gpu_in_chAsq[tidx];
            chBsq_sum += gpu_in_chBsq[tidx];
        }

        // Await assingment in all threads
        __syncthreads();

        // Perform reduction
        chA_sum = block_reduce_sum<T>(chA_sum);
        chB_sum = block_reduce_sum<T>(chB_sum);
        chAsq_sum = block_reduce_sum<T>(chAsq_sum);
        chBsq_sum = block_reduce_sum<T>(chBsq_sum);

        // With the value reduced in the first thread, normalise and output
        if (threadIdx.x == 0) {
            gpu_out_chA[sp] = chA_sum / normalisation;
            gpu_out_chB[sp] = chB_sum / normalisation;
            gpu_out_chAsq[sp] = chAsq_sum / normalisation;
            gpu_out_chBsq[sp] = chBsq_sum / normalisation;
            gpu_out_sq[sp] = (chAsq_sum + chBsq_sum) / normalisation;
        }
    }
}

template <typename T>
void POWER::GPU::power_kernel(
    unsigned int sp_points, unsigned int r_points, T normalisation,
    short *chA_data, short *chB_data,
    POWER::GPU::power_memory<T> memory) {
    /**
     * ==> Ensure that background arrays (set to 0 for no correction) have been copied over
     */
    unsigned int N = sp_points * r_points;
    unsigned int reduction_blocks = POWER::GPU::get_number_of_reduction_blocks(r_points);
    unsigned int shared_memory_size = POWER::GPU::get_shared_memory_size<T>();

    // Copy input data
    cudaMemcpy(memory.gpu_in[CHA], chA_data, N * sizeof(short), cudaMemcpyHostToDevice);
    cudaMemcpy(memory.gpu_in[CHB], chB_data, N * sizeof(short), cudaMemcpyHostToDevice);

    kernel_1
        <<<reduction_blocks, POWER::GPU::threads, shared_memory_size>>>
        (memory.gpu_in[CHA], memory.gpu_in[CHB],
         memory.gpu_aux[CHA], memory.gpu_aux[CHB], memory.gpu_aux[CHASQ], memory.gpu_aux[CHBSQ],
         sp_points, r_points);
    kernel_2
        <<<sp_points, POWER::GPU::threads, shared_memory_size>>>
        (memory.gpu_aux[CHA], memory.gpu_aux[CHB], memory.gpu_aux[CHASQ], memory.gpu_aux[CHBSQ],
         memory.gpu_aux[CHA], memory.gpu_aux[CHB], memory.gpu_aux[CHASQ], memory.gpu_aux[CHBSQ], memory.gpu_aux[SQ],
         sp_points, reduction_blocks, normalisation);

    for (int i(0); i< POWER::no_outputs; i++) {
        cudaMemcpy(memory.data_out[i], memory.gpu_aux[i], sp_points * sizeof(T), cudaMemcpyDeviceToHost);
    }
}

template void POWER::GPU::power_kernel<double>(
    unsigned int sp_points, unsigned int r_points, double normalisation,
    short *chA_data, short *chB_data,
    POWER::GPU::power_memory<double>);
template void POWER::GPU::power_kernel<long>(
    unsigned int sp_points, unsigned int r_points, long normalisation,
    short *chA_data, short *chB_data,
    POWER::GPU::power_memory<long>);
