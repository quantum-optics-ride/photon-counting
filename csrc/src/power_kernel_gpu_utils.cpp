/*
 * Everything that can be compiled without the nvcc for the GPU power kernel
 * - Allocating data on device
 * - Fetching kernel parameters
 */

#include <cuda_runtime.h> // cudaMalloc cudaFree
#include <cuda_runtime_api.h> //for cudaDeviceProp
#include <limits> // For LONG_MAX
#include <string> // For std::to_string

#include "logging.hpp" // RED, OKBLUE etc
#include "power_kernel.hpp" // for power kernel parameters
#include "sp_digitiser.hpp" // for digitiser parameters MAX_DIGITISER_CODE and MAX_NUMBER_OF_RECORDS
#include "utils_gpu.hpp" // To fetch GPU parameters

int check_power_kernel_parameters(bool display){
    PYTHON_START;

    cudaDeviceProp prop = fetch_gpu_parameters(display);

    unsigned long long required_const_memory = SP_POINTS * 2 * sizeof(short);
    if (required_const_memory > prop.totalConstMem)
        FAIL("Power Kernel: Kernel compiled with too many SP_POINTS("
             + std::to_string(SP_POINTS)
             + ") requiring x2 xSHORT("
             + std::to_string(sizeof(short))
             +") = "
             + std::to_string(required_const_memory)
             + "bytes which is bigger than the constant memory available on the GPU ("
             + std::to_string(prop.totalConstMem)
             + "bytes)"
            );

    PYTHON_END;
    return 0;
}

unsigned int POWER::GPU::get_number_of_reduction_blocks(unsigned int r_points) {
    return std::ceil((float)r_points / POWER::GPU::threads);
}

template <typename T>
unsigned int POWER::GPU::get_shared_memory_size() {
    return POWER::GPU::threads * sizeof(T);
}
template unsigned int POWER::GPU::get_shared_memory_size<double>();
template unsigned int POWER::GPU::get_shared_memory_size<long>();

template <typename T>
std::string get_kernel_type_string();
template <> std::string get_kernel_type_string<double>() {return "DOUBLE";}
template <> std::string get_kernel_type_string<long>() {return "LONG";}

template <typename T>
POWER::GPU::power_memory<T> POWER::GPU::allocate_memory(
    unsigned int sp_points, unsigned int r_points) {

    OKBLUE("Power Kernel: Allocating memory on GPU and CPU.");
    POWER::GPU::power_memory<T> memory;

    cudaDeviceProp prop = fetch_gpu_parameters();
    std::string kernel_type = get_kernel_type_string<T>();
    unsigned int N = sp_points * r_points;
    unsigned int reduction_blocks = POWER::GPU::get_number_of_reduction_blocks(r_points);
    int success = 0;

    if (reduction_blocks > (unsigned int)prop.maxGridSize[0])
        FAIL("Too many repetitions points issued!\n" + std::to_string(reduction_blocks) + " > " + std::to_string(prop.maxGridSize[0]));
    if (sp_points > (unsigned int)prop.maxGridSize[0])
        FAIL("Too many sp points issued!\n" + std::to_string(sp_points) + " > " + std::to_string(prop.maxGridSize[0]));

    const unsigned long shared_memory_required = (
        POWER::GPU::get_shared_memory_size<T>() * POWER::GPU::no_outputs_from_gpu
        );
    if (prop.sharedMemPerBlock < shared_memory_required)
        FAIL(
            "Power Kernel: Not enough shared memory reserved GPU for "
            + kernel_type + " Kernel\n\tTHREADS("
            + std::to_string(POWER::GPU::threads)
            + ") x arrays used in GPU("
            + std::to_string(POWER::GPU::no_outputs_from_gpu)
            + ") x " + kernel_type + "(" + std::to_string(sizeof(T))
            + ") = "
            + std::to_string(shared_memory_required)
            + "> "
            + std::to_string(prop.sharedMemPerBlock)
            + " bytes availabel per GPU block.");

    // Ensure that the memory being allocated will be large anough to sum up all repetitions
    // Max value will occur for CHA^2 + CHB^2
    if (2 * r_points * MAX_DIGITISER_CODE * MAX_DIGITISER_CODE > std::numeric_limits<T>::max())
        FAIL(
            "MAX SQUARE VALUE == No repetitions ("
            + std::to_string(r_points)
            + ") x 14bit Code ("
            + std::to_string(MAX_DIGITISER_CODE)
            + ")^2 x 2 could overflow the cumulaitve arrays on the GPU");

    // Check that global memory is not exceeded
    unsigned long gpu_in_size = N * sizeof(short);
    unsigned long gpu_aux_size = sp_points * reduction_blocks * sizeof(T);

    long gpu_global_memory_allocation = (
        2 * gpu_in_size
        + gpu_aux_size * POWER::no_outputs
        );
    if (gpu_global_memory_allocation > (long)prop.totalGlobalMem)
        FAIL(
            "Input arrays for chA and chB of type" + std::string("(short) and ")
            + std::to_string(POWER::GPU::no_outputs_from_gpu) + "x auxillary arrays " + kernel_type +
            + " allocated in the gpu (" + std::to_string(gpu_global_memory_allocation)
            + "bytes) bigger than global memory on GPU (" + std::to_string(prop.totalGlobalMem) + "bytes)."
            );

    success += cudaMalloc(reinterpret_cast<void**>(&memory.gpu_in[CHA]), N * sizeof(short));
    success += cudaMalloc(reinterpret_cast<void**>(&memory.gpu_in[CHB]), N * sizeof(short));
    for (int i(0); i< POWER::no_outputs; i++)
        success += cudaMalloc(reinterpret_cast<void**>(&memory.gpu_aux[i]), sp_points * reduction_blocks * sizeof(T));
    if (success != 0) FAIL("Power Kernel: Failed to allocate memory on GPU!");

    for (int i(0); i < POWER::no_outputs; i++)
        memory.data_out[i] = new T[sp_points]();

    OKGREEN("Power Kernel: Allocation done!");
    return memory;
}
template POWER::GPU::power_memory<double> POWER::GPU::allocate_memory<double>(unsigned int sp_points, unsigned int r_points);
template POWER::GPU::power_memory<long> POWER::GPU::allocate_memory<long>(unsigned int sp_points, unsigned int r_points);

template <typename T>
void POWER::GPU::free_memory(POWER::GPU::power_memory<T> memory) {
    OKBLUE("Power Kernel: Deallocating memory on GPU and CPU.");
    int success = 0;

    cudaFree(memory.gpu_in[CHA]);
    cudaFree(memory.gpu_in[CHB]);
    for (int i(0); i < POWER::no_outputs; i++) {
        success += cudaFree(memory.gpu_aux[i]);
    }
    if (success != 0) FAIL("Power Kernel: Failed to free memory on GPU!");

    for (int i(0); i < POWER::no_outputs; i++)
        delete[] memory.data_out[i];

    OKGREEN("Power Kernel: Memory freed!");
}
template void POWER::GPU::free_memory<double>(POWER::GPU::power_memory<double> memory);
template void POWER::GPU::free_memory<long>(POWER::GPU::power_memory<long> memory);
