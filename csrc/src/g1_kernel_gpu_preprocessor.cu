#include <helper_cuda.h> // for CUDA_CHECK

#include "logging.hpp"
#include "g1_kernel.hpp"
#include "reduction_gpu.cuh"

void G1::GPU::copy_background_arrays_to_gpu(short *chA_background, short *chB_background, G1::GPU::g1_memory memory, int N) {
    int success = 0;

    if (N > G1_DIGITISER_POINTS) FAIL("Background arrays ("
                                      + std::to_string(N)
                                      + ") larger than the G1_DIGITISER_POINTS("
                                      + std::to_string(G1_DIGITISER_POINTS)
                                      + ") requested from the digitiser and that the kernel was compiled with."
        );

    success += cudaMemcpy(memory.gpu_background[CHAG1], chA_background,
                          N * sizeof(short), cudaMemcpyHostToDevice);
    success += cudaMemcpy(memory.gpu_background[CHBG1], chB_background,
                          N * sizeof(short), cudaMemcpyHostToDevice);
    if (success != 0) FAIL("Failed to copy background data TO the GPU.");
}

/**
 * Summation of `gpu_in` into `gpu_out` with optional normalisation.
 *
 * `gpu_in` is subdivided into blocks:
 * [b1, b1, b1, b1, ..., b2, b2, b2, b2, ...]
 *
 * The length of `gpu_out` must be at least as large as the number of blocks
 * the kernel was called with in order to retain the values reduced in each of the blocks:
 * [block1sum, block2sum, block3sum, ...]
 *
 * **This will normally be used as a second invocation after `mean_kernel` or `variance_and_normalisation_kernel`
 *  in order to reduce their temporary outputs.**
 */
template <typename Tin, typename Tout>
__global__ void reduction_kernel(
    Tin *gpu_in_chA, Tin *gpu_in_chB, Tin *gpu_in_sq,
    Tout *gpu_out,
    unsigned int N,
    Tout normalisation

    ) {

    Tout chA_sum(0), chB_sum(0), sq_sum(0);
    // Copy over input data to `sum` that is local to this thread.
    for (int tidx = blockIdx.x * blockDim.x + threadIdx.x;
         tidx < N;
         tidx += blockDim.x * gridDim.x) {
        chA_sum += (Tout)gpu_in_chA[tidx];
        chB_sum += (Tout)gpu_in_chB[tidx];
        sq_sum += (Tout)gpu_in_sq[tidx];
    }

    // Perform reduction
    chA_sum = block_reduce_sum<Tout>(chA_sum);
    chB_sum = block_reduce_sum<Tout>(chB_sum);
    sq_sum = block_reduce_sum<Tout>(sq_sum);

    // With the value reduced in the first thread, normalise and output
    if (threadIdx.x == 0) {
        gpu_out[CHAG1] = chA_sum / normalisation;
        gpu_out[CHBG1] = chB_sum / normalisation;
        gpu_out[SQG1] = sq_sum / normalisation;
    }
}

/**
 * - Takes away background from CHA and CHB
 * - The blocks of N points are reduced into blocks: [block1sum, block2sum, block3sum, ...]
 *   that need to be reduced one more time.
 */
template <typename T>
__global__ void mean_kernel(
    short *gpu_in_chA, short *gpu_in_chB, short *gpu_background_chA, short *gpu_background_chB,
    T *gpu_fftw_aux_chA, T *gpu_fftw_aux_chB, T *gpu_fftw_aux_sq,
    unsigned int N
    ) {

    T chA_sum(0), chB_sum(0), sq_sum(0);
    T _chA, _chB;
    // Copy over input data to `sum` that is local to this thread.
    for (int tidx = blockIdx.x * blockDim.x + threadIdx.x;
         tidx < N;
         tidx += blockDim.x * gridDim.x) {

        gpu_in_chA[tidx] -= gpu_background_chA[tidx];
        gpu_in_chB[tidx] -= gpu_background_chB[tidx];

        _chA = (T)gpu_in_chA[tidx];
        _chB = (T)gpu_in_chB[tidx];

        chA_sum += _chA;
        chB_sum += _chB;
        sq_sum += _chA * _chA + _chB * _chB;
    }

    // Perform reduction
    chA_sum = block_reduce_sum<T>(chA_sum);
    chB_sum = block_reduce_sum<T>(chB_sum);
    sq_sum = block_reduce_sum<T>(sq_sum);

    // With the value reduced in the first thread, normalise and output
    if (threadIdx.x == 0) {
        gpu_fftw_aux_chA[blockIdx.x] = chA_sum;
        gpu_fftw_aux_chB[blockIdx.x] = chB_sum;
        gpu_fftw_aux_sq[blockIdx.x] = sq_sum;
    }
}

/**
 * Alternative copying of data compared to the reduction_kernel with:
 * - Normalisation of input data by the mean
 * - Storage of the mean square deviation in order to compute the variance.
 * Returns sum in the individual blocks: [block1sum, block2sum, block3sum, ...] that need to be reduced one more time.
 */
template <typename Tin, typename Tout>
__global__ void variance_and_normalisation_kernel(
    Tin *gpu_in_chA, Tin *gpu_in_chB,
    Tout *gpu_fftw_aux_chA, Tout *gpu_fftw_aux_chB, Tout *gpu_fftw_aux_sq,
    Tout *gpu_normalised_chA, Tout *gpu_normalised_chB, Tout *gpu_normalised_sq,
    Tout *gpu_mean,
    unsigned int N
    ) {

    Tout chA_sum(0), chB_sum(0), sq_sum(0);
    Tout _chA, _chB;

    // Normalise by the mean and seed the `sum` for this thread
    // in order to sum up the square differences for evaluation of the variance.
    for (int tidx = blockIdx.x * blockDim.x + threadIdx.x;
         tidx < N;
         tidx += blockDim.x * gridDim.x) {

        _chA = (Tout)gpu_in_chA[tidx];
        _chB = (Tout)gpu_in_chB[tidx];

        gpu_normalised_chA[tidx] = _chA - gpu_mean[CHAG1];
        gpu_normalised_chB[tidx] = _chB - gpu_mean[CHBG1];
        gpu_normalised_sq[tidx] = _chA * _chA + _chB * _chB - gpu_mean[SQG1];

        chA_sum += gpu_normalised_chA[tidx] * gpu_normalised_chA[tidx];
        chB_sum += gpu_normalised_chB[tidx] * gpu_normalised_chB[tidx];
        sq_sum += gpu_normalised_sq[tidx] * gpu_normalised_sq[tidx];
    }

    // Perform reduction
    chA_sum = block_reduce_sum<Tout>(chA_sum);
    chB_sum = block_reduce_sum<Tout>(chB_sum);
    sq_sum = block_reduce_sum<Tout>(sq_sum);

    // With the value reduced in the first thread, normalise and output
    if (threadIdx.x == 0){
        gpu_fftw_aux_chA[blockIdx.x] = chA_sum;
        gpu_fftw_aux_chB[blockIdx.x] = chB_sum;
        gpu_fftw_aux_sq[blockIdx.x] = sq_sum;
    }
}

template <typename T>
void G1::GPU::preprocessor(int N,
                           short *chA_data, short *chB_data,
                           short **gpu_raw_data, short **gpu_background, T **gpu_normalised,
                           T **gpu_pp_aux, T *gpu_mean, T *gpu_variance) {
    const unsigned long blocks = G1::GPU::get_number_of_gpu_blocks(N);

    /** ==> Ensure that copy_background_arrays_to_gpu has been called to transfer background data */

    // Copy input data
    cudaMemcpy(gpu_raw_data[CHAG1], chA_data, N * sizeof(short), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu_raw_data[CHBG1], chB_data, N * sizeof(short), cudaMemcpyHostToDevice);

    // Evaluation of mean
    mean_kernel
        <<<blocks, G1::GPU::pp_threads, G1::GPU::pp_shared_memory * sizeof(T)>>>
        (gpu_raw_data[CHAG1], gpu_raw_data[CHBG1],
         gpu_background[CHAG1], gpu_background[CHBG1],
         gpu_pp_aux[CHAG1], gpu_pp_aux[CHBG1], gpu_pp_aux[SQG1], N);
    reduction_kernel
        <<<1, G1::GPU::pp_threads, G1::GPU::pp_shared_memory>>>
        (gpu_pp_aux[CHAG1], gpu_pp_aux[CHBG1], gpu_pp_aux[SQG1], gpu_mean, blocks, (T)N);

    // Evaluation of variance and normalisation
    variance_and_normalisation_kernel
        <<<blocks, G1::GPU::pp_threads, G1::GPU::pp_shared_memory *sizeof(T)>>>
        (gpu_raw_data[CHAG1], gpu_raw_data[CHBG1],
         gpu_pp_aux[CHAG1], gpu_pp_aux[CHBG1], gpu_pp_aux[SQG1],
         gpu_normalised[CHAG1], gpu_normalised[CHBG1], gpu_normalised[SQG1],
         gpu_mean, N);
    reduction_kernel
        <<<1, G1::GPU::pp_threads, G1::GPU::pp_shared_memory>>>
        (gpu_pp_aux[CHAG1], gpu_pp_aux[CHBG1], gpu_pp_aux[SQG1], gpu_variance, blocks, (T)N);
}

template <typename T>
void G1::GPU::preprocessor(int N,
                           short *chA_data, short *chB_data,
                           short **gpu_raw_data, short **gpu_background, T **gpu_normalised,
                           T **gpu_pp_aux, T *gpu_mean, T *gpu_variance,
                           T *mean_list, T *variance_list, T **normalised_data){
    G1::GPU::preprocessor(N, chA_data, chB_data,
                          gpu_raw_data, gpu_background, gpu_normalised,
                          gpu_pp_aux, gpu_mean, gpu_variance);

    // After preprocessing on GPU, dump results back to CPU.
    cudaMemcpy(mean_list, gpu_mean, G1::no_outputs * sizeof(T), cudaMemcpyDeviceToHost);
    cudaMemcpy(variance_list, gpu_variance, G1::no_outputs * sizeof(T), cudaMemcpyDeviceToHost);
    cudaMemcpy(normalised_data[CHAG1], gpu_normalised[CHAG1], N * sizeof(T), cudaMemcpyDeviceToHost);
    cudaMemcpy(normalised_data[CHBG1], gpu_normalised[CHBG1], N * sizeof(T), cudaMemcpyDeviceToHost);
    cudaMemcpy(normalised_data[SQG1], gpu_normalised[SQG1], N * sizeof(T), cudaMemcpyDeviceToHost);
}

template void G1::GPU::preprocessor<float>(
    int N, short *chA_data, short *chB_data,
    short **gpu_raw_data, short **gpu_background, float **gpu_normalised,
    float **gpu_pp_aux, float *gpu_mean, float *gpu_variance,
    float *mean_list, float *variance_list, float **normalised_data
    );
template void G1::GPU::preprocessor<double>(
    int N, short *chA_data, short *chB_data,
    short **gpu_raw_data, short **gpu_background, double **gpu_normalised,
    double **gpu_pp_aux, double *gpu_mean, double *gpu_variance,
    double *mean_list, double *variance_list, double **normalised_data);
