#include <celero/Celero.h>
#include <cstdlib> // srand
#include <ctime> // time
#include <cmath>

#include "ADQAPI.h" // DeleteAdqControlUnit

#include "logging.hpp"
#include "utils.hpp"
#include "sp_digitiser.hpp" // fetch_digitiser_data
#include "power_kernel.hpp"
#include "power_pipeline.hpp"
#include "g1_kernel.hpp"
#include "g1_pipeline.hpp"

CELERO_MAIN

///////////////////////////////////////////////////////////////////////////////
//                              Digitiser                                    //
///////////////////////////////////////////////////////////////////////////////
const int digitiser_code_range = std::pow(2, 14);
short digitiser_code() {
    return ((float)std::rand() / RAND_MAX - 0.5) * digitiser_code_range;
}

class DigitiserFixture : public celero::TestFixture {
public:
    void* adq_cu_ptr;
    short *buff_a; short *buff_b;

    void setUp(__attribute__ ((unused)) const celero::TestFixture::ExperimentValue& x) override {

        // Create pointer and set up device for multirecord
        adq_cu_ptr = master_setup(NO_BLINK,
                                  INTERNAL_CLOCK_SOURCE_INTERNAL_10MHZ_REFFERENCE,
                                  TRIGGER_EXTERNAL
                                  // TRIGGER_SOFTWARE
            );

        // Simulate with real number of points that we would typically expect
        buff_a = new short[get_sp_points() * get_r_points()];
        buff_b = new short[get_sp_points() * get_r_points()];

        // Prepare multirecord mode
        if (!ADQ_MultiRecordSetup(adq_cu_ptr, 1, get_r_points(), get_sp_points())) FAIL("Failed multirecord setup");
    };

    void tearDown() override {
        ADQ_MultiRecordClose(adq_cu_ptr, 1);
        delete[] buff_a; delete[] buff_b;
        DeleteADQControlUnit(adq_cu_ptr);
    };

    virtual unsigned int get_r_points() = 0;
    virtual unsigned int get_sp_points() = 0;
};

///////////////////////////////////////////////////////////////////////////////
//                                Power Kernel                               //
///////////////////////////////////////////////////////////////////////////////
const unsigned int r_points = 128000;
const unsigned int sp_points = 400;

class PowerDigitiserFixture : public DigitiserFixture {
public:
    unsigned int get_r_points() override {
        return r_points;
    }
    unsigned int get_sp_points() override {
        return sp_points;
    }
};

class PowerKernelFixture : public celero::TestFixture {
public:
    short chA_data[sp_points * r_points];
    short chB_data[sp_points * r_points];
    double *data_out[POWER::no_outputs];

    short chA_background[sp_points];
    short chB_background[sp_points];

    void* adq_cu_ptr;

    void setUp(__attribute__ ((unused)) const celero::TestFixture::ExperimentValue& x) override {
        // Seed generator for population of arrays
        std::srand(std::time(0));
        for (unsigned int i(0); i < sp_points * r_points; i++) {
            chA_data[i] = digitiser_code();
            chB_data[i] = digitiser_code();
        }
        for (int i(0); i < POWER::no_outputs; i++)
            data_out[i] = new double[sp_points];
        for (unsigned int i(0); i < sp_points; i++) {
            chA_background[i] = digitiser_code();
            chB_background[i] = digitiser_code();
        }

        adq_cu_ptr = master_setup(NO_BLINK,
                                  INTERNAL_CLOCK_SOURCE_INTERNAL_10MHZ_REFFERENCE,
                                  TRIGGER_EXTERNAL
                                  // TRIGGER_SOFTWARE
            );
    };
    void tearDown() override {
        for (int i(0); i < POWER::no_outputs; i++)
            delete[] data_out[i];
        DeleteADQControlUnit(adq_cu_ptr);
    };
};

class PowerKernelGPUBaseFixture : public PowerKernelFixture {
public:

    POWER::GPU::power_memory<long> memory;
    long *cumulative[POWER::no_outputs];

    void setUp(__attribute__ ((unused)) const celero::TestFixture::ExperimentValue& x) override {
        PowerKernelFixture::setUp(x);

        POWER::GPU::copy_background_arrays_to_gpu(chA_background, chB_background);
        memory = POWER::GPU::allocate_memory<long>(sp_points, r_points);
        for (int i(0); i < POWER::no_outputs; i++)
            cumulative[i] = new long[sp_points];
    };

    void tearDown() override {
        for (int i(0); i < POWER::no_outputs; i++)
            delete[] cumulative[i];
        PowerKernelFixture::tearDown();
        POWER::GPU::free_memory(memory);
    };
};

// BASELINE_F(POWER, READING, PowerDigitiserFixture, 0, 0)
// {
//     fetch_digitiser_data(adq_cu_ptr,
//                          buff_a, buff_b,
//                          get_sp_points(), get_r_points());
// }
// BENCHMARK_F(POWER, KERNEL_CPU, PowerKernelFixture, 0, 0) {
//     int no_threads = 1;

//     POWER::CPU::power_kernel(
//         chA_data, chB_data, data_out,
//         chA_background, chB_background,
//         sp_points, r_points, no_threads);
// }
// BENCHMARK_F(POWER, KERNEL_GPU, PowerKernelGPUBaseFixture, 0, 0) {
//     POWER::GPU::power_kernel(sp_points, r_points, (long)1, chA_data, chB_data, memory);
// }
// BENCHMARK_F(POWER, PROCESSING_GPU, PowerKernelGPUBaseFixture, 0, 0) {
//     process_power_digitiser_data(
//         sp_points, r_points,
//         chA_data, chB_data,
//         memory,
//         "./dump/bench_processing-example", cumulative, (unsigned long)1);
// }
// BENCHMARK_F(POWER, PIPELINE_GPU, PowerKernelGPUBaseFixture, 0, 0) {
//     run_power_measurements(
//         adq_cu_ptr, chA_background, chB_background, sp_points, r_points,
//         "./dump/power-pipeline");
// }

///////////////////////////////////////////////////////////////////////////////
//                                G1 Benchmark                               //
///////////////////////////////////////////////////////////////////////////////
const int tau_points = 100;
short chA_background_g1[G1_DIGITISER_POINTS];
short chB_background_g1[G1_DIGITISER_POINTS];

class G1DigitiserFixture : public DigitiserFixture {
public:
    unsigned int get_r_points() override {
        return 1;
    }
    unsigned int get_sp_points() override {
        return G1_DIGITISER_POINTS;
    }
};

class G1KernelFixture : public celero::TestFixture {
public:
    short chA_data[G1_DIGITISER_POINTS];
    short chB_data[G1_DIGITISER_POINTS];
    double **data_out = new double*[G1::no_outputs];

    // For the CPU preprocessor
    double chA_wip[G1_DIGITISER_POINTS]; double chB_wip[G1_DIGITISER_POINTS]; double sq_wip[G1_DIGITISER_POINTS];
    double *wip_data[G1::no_outputs] = {chA_wip, chB_wip, sq_wip};
    double mean_list[G1::no_outputs];
    double variance_list[G1::no_outputs];

    void setUp(__attribute__ ((unused)) const celero::TestFixture::ExperimentValue& x) override {
        // Seed generator for population of arrays
        std::srand(std::time(0));
        for (int i(0); i < G1_DIGITISER_POINTS; i++) {
            chA_data[i] = digitiser_code();
            chB_data[i] = digitiser_code();
        }
        for (int i(0); i < G1::no_outputs; i++)
            data_out[i] = new double[tau_points]();
    };
    void tearDown() override {
        for (int i(0); i < G1::no_outputs; i++)
            delete[] data_out[i];
        delete[] data_out;
    };
};

class G1KernelFixture_CPU_FFTW : public G1KernelFixture  {
public:
    fftw_complex **aux_array;
    fftw_plan *plans_forward, *plans_backward;
    double **data_out_fftw;

    virtual std::string get_plan_name() = 0;

    void setUp(__attribute__ ((unused)) const celero::TestFixture::ExperimentValue& x) override {
        G1KernelFixture::setUp(x);

        G1::CPU::FFTW::g1_allocate_memory(
            data_out_fftw, aux_array, "./dump/bench-1-thread-plan",
            plans_forward, plans_backward);
    };
    void tearDown() override {
        G1KernelFixture::tearDown();
        G1::CPU::FFTW::g1_free_memory(data_out_fftw, aux_array, plans_forward, plans_backward);
    };
};

class G1KernelFixture_CPU_FFTW_1Threads : public G1KernelFixture_CPU_FFTW {
public:
    std::string get_plan_name() override {
        return "../dump/bench-1-thread-plan";
    }
};

class G1KernelFixture_CPU_FFTW_2Threads : public G1KernelFixture_CPU_FFTW {
public:
    std::string get_plan_name() override {
        return "bench-2-thread-plan";
    }
};
class G1KernelFixture_CPU_FFTW_4Threads : public G1KernelFixture_CPU_FFTW {
public:
    std::string get_plan_name() override {
        return "bench-4-thread-plan";
    }
};
class G1KernelFixture_CPU_FFTW_8Threads : public G1KernelFixture_CPU_FFTW {
public:
    std::string get_plan_name() override {
        return "bench-8-thread-plan";
    }
};

class G1KernelFixture_GPU : public G1KernelFixture {
public:
    cufftHandle *plans_forward; cufftHandle *plans_backward;
    G1::GPU::g1_memory memory;
    float *cumulative[G1::no_outputs];

    void setUp(__attribute__ ((unused)) const celero::TestFixture::ExperimentValue& x) override {

        G1::GPU::g1_prepare_fftw_plan(plans_forward, plans_backward);

        memory = G1::GPU::allocate_memory();

        for (int i(0); i < G1::no_outputs; i++)
            cumulative[i] = new float[G1_DIGITISER_POINTS]();

        G1::check_g1_kernel_parameters(false);
    };

    void tearDown() override {
        G1::GPU::free_memory(memory);

        for (int i(0); i < G1::no_outputs; i++)
            delete[] cumulative[i];
    };
};

BASELINE_F(G1, READING, G1DigitiserFixture, 1, 1)
{
    fetch_digitiser_data(adq_cu_ptr,
                         buff_a, buff_b,
                         get_sp_points(), get_r_points());
}
BENCHMARK_F(G1, KERNEL_CPU_DIRECT_1T, G1KernelFixture, 0, 0) {
    int no_threads = 1;
    G1::CPU::DIRECT::g1_kernel(chA_data, chB_data, data_out, tau_points, false, no_threads);
}
BENCHMARK_F(G1, KERNEL_CPU_FFTW_1T, G1KernelFixture_CPU_FFTW_1Threads, 0, 0)
{
    G1::CPU::FFTW::g1_kernel(chA_data, chB_data,
                             data_out_fftw, aux_array,
                             plans_forward, plans_backward);
}
BENCHMARK_F(G1, KERNEL_GPU_FFTW, G1KernelFixture_GPU, 0, 0)
{
    G1::GPU::g1_kernel(chA_data, chB_data,
                       memory,
                       plans_forward, plans_backward);
}
BENCHMARK_F(G1, PREPROCESS_CPU, G1KernelFixture, 0, 0)
{
    G1::CPU::preprocessor(chA_data, chB_data, G1_DIGITISER_POINTS, mean_list, variance_list, wip_data);
}
BENCHMARK_F(G1, PREPROCESSOR_GPU, G1KernelFixture_GPU, 0, 0)
{
    G1::GPU::preprocessor(
        G1_DIGITISER_POINTS, chA_data, chB_data,
        memory.gpu_raw_data, memory.gpu_background, reinterpret_cast<float**>(memory.gpu_inout),
        memory.gpu_pp_aux, memory.gpu_mean, memory.gpu_variance);
}
BENCHMARK_F(G1, PROCESS_GPU, G1KernelFixture_GPU, 0, 0)
{
    process_g1_digitiser_data(chA_data, chB_data,
                              memory,
                              plans_forward, plans_backward,
                              "./dump/g1-benchmakrk-pipeline",
                              cumulative, 1, tau_points);
}
BENCHMARK_F(G1, PIPELINE_GPU_1R, G1DigitiserFixture, 0, 0)
{
    int no_runs = 1;
    run_g1_measurements(adq_cu_ptr,
                        chA_background_g1, chB_background_g1,
                        no_runs, "./dump/g1-pipeline-full", tau_points);
}
BENCHMARK_F(G1, PIPELINE_GPU_2R, G1DigitiserFixture, 30, 5)
{
    int no_runs = 2;

    run_g1_measurements(adq_cu_ptr,
                        chA_background_g1, chB_background_g1,
                        no_runs, "./dump/g1-pipeline-full", tau_points);
}
BENCHMARK_F(G1, PIPELINE_GPU_3R, G1DigitiserFixture, 30, 5)
{
    int no_runs = 3;

    run_g1_measurements(adq_cu_ptr,
                        chA_background_g1, chB_background_g1,
                        no_runs, "./dump/g1-pipeline-full", tau_points);
}
BENCHMARK_F(G1, PIPELINE_GPU_4R, G1DigitiserFixture, 30, 5)
{
    int no_runs = 4;

    run_g1_measurements(adq_cu_ptr,
                        chA_background_g1, chB_background_g1,
                        no_runs, "./dump/g1-pipeline-full", tau_points);
}
// BENCHMARK_F(G1, PIPELINE_GPU_5R, G1DigitiserFixture, 30, 5)
// {
//     int no_runs = 5;

//     run_g1_measurements(adq_cu_ptr, no_runs, "./dump/g1-pipeline-full", 1000);
// }
// BENCHMARK_F(G1, PIPELINE_GPU_1000R, G1DigitiserFixture, 30, 1)
// {
//     int no_runs = 1000;

//     run_g1_measurements(adq_cu_ptr, no_runs, "./dump/g1-pipeline-full", 1000);
// }
